# coding=utf-8
from django.conf import settings
from django.core.urlresolvers import resolve

def contex_core(request):
    return {'site_url': settings.SITE_URL,
            'site_name': settings.SITE_NAME,
            'current_url': resolve(request.path_info).url_name,
            'current_path': request.path_info,
            'support_email_address': settings.SUPPORT_EMAIL_ADDRESS,}
