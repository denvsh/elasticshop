# coding=utf-8
# author: dlyapun

from django.conf.urls import url, include
from force_shop.views import *
from force_shop.api import *


urlpatterns = [
    url(r'^$', ListProductView.as_view(), name="list-shop-url"),
    url(r'^order/$', OrderView.as_view(), name="order"),
    url(r'^pay/$', PayOrderView.as_view(), name="pay"),
    url(r'^cart/$', CartView.as_view(), name="cart-page-url"),
    url(r'^add_to_cart/$', AddToCartView.as_view(), name='add-to-cart-url'),
    url(r'^remove/(?P<product_id>[\w-]+)/$', RemoveProductFromCart.as_view(), name='remove-product-cart-url'),
    url(r'^(?P<meta_url>[\w-]+)/$', DetailProductView.as_view(), name="detail-product-url"),
    url(r'^category/(?P<category_slug>[\w-]+)/$', CategoryListView.as_view(), name="category-list-url"),
    url(r'^search$', SearchProductView.as_view(), name='product-search')
]

# API
urlpatterns += [
    url(r'^api/v1/products-list/$', ProductListView.as_view(), name='products-list-api'),
    url(r'^api/v1/product-add/$', ProductAddView.as_view(), name='product-add-api'),
    url(r'^api/v1/product-remove/$', ProductRemoveView.as_view(), name='product-remove-api'),
]